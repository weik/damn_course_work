import 'package:flutter/foundation.dart';
import 'package:hive/hive.dart';

part 'meal.g.dart';

enum Complexity {
  Simple,
  Challenging,
  Hard,
}
enum Affordability {
  Affordable,
  Pricey,
  Luxurious,
}

@HiveType(typeId: 1)
class Meal extends HiveObject {
  @HiveField(0)
  final String id;
  @HiveField(1)
  final List<String> categories;
  @HiveField(2)
  final String title;
  @HiveField(3)
  final String imageUrl;
  @HiveField(4)
  final List<String> ingredients;
  @HiveField(5)
  final List<String> steps;
  @HiveField(6)
  final int duration;
  @HiveField(7)
  final String complexity;
  @HiveField(8)
  final String affordability;
  @HiveField(9)
  final bool isGlutenFree;
  @HiveField(10)
  final bool isLactoseFree;
  @HiveField(11)
  final bool isVegan;
  @HiveField(12)
  final bool isVegetarian;

  Meal({
    @required this.affordability,
    @required this.complexity,
    @required this.title,
    @required this.duration,
    @required this.id,
    @required this.categories,
    @required this.imageUrl,
    @required this.ingredients,
    @required this.isGlutenFree,
    @required this.isLactoseFree,
    @required this.isVegan,
    @required this.isVegetarian,
    @required this.steps,
  });

  factory Meal.fromJson(Map<dynamic, dynamic> json) {
    return Meal(
      affordability: json['affordability'],
      duration: json['duration'],
      categories: json['categories'],
      complexity: json['complexity'],
      title: json['title'],
      id: json['id'],
      imageUrl: json['imageUrl'],
      ingredients: json['ingredients'],
      isGlutenFree: json['isGlutenFree'],
      isLactoseFree: json['isLactoseFree'],
      isVegan: json['isVegan'],
      isVegetarian: json['isVegetarian'],
      steps: json['steps'],
    );
  }
}
