import 'package:hive/hive.dart';
import 'package:meals_app/model/category.dart';
import 'package:meals_app/model/meal.dart';

part 'hive_model.g.dart';

@HiveType(typeId: 0)
class HiveModel extends HiveObject {
  HiveModel({this.categoryList, this.mealsList});

  @HiveField(0)
  List<Category> categoryList;
  @HiveField(1)
  List<Meal> mealsList;

  factory HiveModel.fromJson(Map<dynamic, dynamic> json) {
    return HiveModel(
      categoryList: json['categoryList'],
      mealsList: json['mealsList'],
    );
  }
}
