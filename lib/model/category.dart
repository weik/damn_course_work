import 'package:flutter/material.dart';
import 'package:hive/hive.dart';

part 'category.g.dart';

@HiveType(typeId: 2)
class Category {
  @HiveField(0)
  final String id;
  @HiveField(1)
  final String title;
  @HiveField(2)
  final String color;

  const Category({
    this.color,
    @required this.id,
    @required this.title,
  });

  factory Category.fromJson(Map<dynamic, dynamic> json) {
    return Category(
      color: json['color'],
      id: json['id'],
      title: json['title'],
    );
  }
}
