import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:meals_app/data/dummy_data.dart';
import 'package:meals_app/model/category.dart';
import 'package:meals_app/model/hive_model.dart';
import 'package:meals_app/providers/hive_provider.dart';
import 'package:meals_app/providers/meals_details_provider.dart';
import 'package:meals_app/repository/meals_repository.dart';
import 'package:meals_app/screens/favourite/favourite_meal_screen.dart';
import 'package:meals_app/screens/meals/meals_detail_screen.dart';
import 'package:meals_app/screens/settings/settings_screen.dart';
import 'package:meals_app/splash_screen.dart';
import 'package:provider/provider.dart';

import 'constants.dart';
import 'model/meal.dart';
import 'screens/home/home_screen.dart';
import 'screens/meals/meals_screen.dart';

bool isDarkMode = false;

void main() async {
  await Hive.initFlutter();
  WidgetsFlutterBinding.ensureInitialized();
  Hive.registerAdapter(HiveModelAdapter());
  Hive.registerAdapter(CategoryAdapter());
  Hive.registerAdapter(MealAdapter());
  //Hive.openBox<HiveModel>('meals');

  final mealsRepo = MealsRepository();
  runApp(MultiProvider(providers: [
    ChangeNotifierProvider(
      create: (_) => MealsDetailsProvider(),
    ),
    ChangeNotifierProvider(
      create: (_) => HiveProvider(
        mealsRepository: mealsRepo,
      ),
    )
  ], child: MyApp()));
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  Map<String, bool> _filters = {
    'gluten': false,
    'lactose': false,
    'vegan': false,
    'vegetarian': false,
  };

  List<Meal> _availableMeals = dummyMeals;

  List<Meal> _favoriteMeals = [];

  void _setFilters(Map<String, bool> filterData) {
    setState(() {
      _filters = filterData;

      _availableMeals = dummyMeals.where((meal) {
        if (_filters['gluten'] && !meal.isGlutenFree) {
          return false;
        }
        if (_filters['lactose'] && !meal.isLactoseFree) {
          return false;
        }
        if (_filters['vegan'] && !meal.isVegan) {
          return false;
        }
        if (_filters['vegetarian'] && !meal.isVegetarian) {
          return false;
        }
        return true;
      }).toList();
    });
  }

  void _toggleFavorite(String mealId) {
    final existingIndex =
        _favoriteMeals.indexWhere((meal) => meal.id == mealId);
    if (existingIndex >= 0) {
      setState(() {
        _favoriteMeals.removeAt(existingIndex);
      });
    } else {
      setState(() {
        _favoriteMeals.add(
          dummyMeals.firstWhere((meal) => meal.id == mealId),
        );
      });
    }
  }

  bool _isMealFavorite(String id) {
    return _favoriteMeals.any((meal) => meal.id == id);
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: appName,
      theme: isDarkMode
          ? ThemeData.dark().copyWith(
              //isDarkMode? Colors.black : primarySwatch[700]
              brightness: Brightness.dark,
              accentColor: Colors.amber,
              //canvasColor: const Color.fromRGBO(255, 254, 229, 1),
              textTheme: ThemeData.dark().textTheme.copyWith(
                    bodyText2: const TextStyle(
                      color: Colors.white70,
                    ),
                    bodyText1: const TextStyle(
                      color: Colors.white70,
                    ),
                    headline6: const TextStyle(
                      fontSize: 20,
                      fontFamily: 'RobotoCondensed',
                      fontWeight: FontWeight.bold,
                    ),
                  ),
            )
          : ThemeData.light().copyWith(
              brightness: Brightness.light,
              primaryColor: Colors.blueGrey,
              accentColor: Colors.amber,
              canvasColor: const Color.fromRGBO(255, 254, 229, 1),
              textTheme: ThemeData.light().textTheme.copyWith(
                    bodyText2: const TextStyle(
                      color: Color.fromRGBO(20, 51, 51, 1),
                    ),
                    bodyText1: const TextStyle(
                      color: Color.fromRGBO(20, 51, 51, 1),
                    ),
                    headline6: const TextStyle(
                      fontSize: 20,
                      fontFamily: 'RobotoCondensed',
                      fontWeight: FontWeight.bold,
                    ),
                  ),
            ),
      initialRoute: SplashScreen.id,
      routes: {
        SplashScreen.id: (_) => SplashScreen(),
        HomeScreen.id: (_) => HomeScreen(),
        MealsScreen.id: (_) => MealsScreen(_availableMeals),
        SettingsScreen.id: (_) => SettingsScreen(
              currentFilters: _filters,
              saveFilters: _setFilters,
            ),
        FavouriteMealScreen.id: (_) => FavouriteMealScreen(
              favouriteMeals: _favoriteMeals,
            ),
        MealsDetailScreen.id: (_) => MealsDetailScreen(
              toggleFavorite: _toggleFavorite,
              isFavorite: _isMealFavorite,
            ),
      },
    );
  }
}
