import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

class MealsDetailsProvider extends ChangeNotifier {
  FixedExtentScrollController fixedExtentIngredientScrollController;
  FixedExtentScrollController fixedExtentStepScrollController;
  int currentIngredientItem = 1;
  int currentStepItem = 0;

  FixedExtentScrollController initializeStepScrollController() {
    return fixedExtentStepScrollController = FixedExtentScrollController(
      initialItem: currentStepItem,
    );
  }

  FixedExtentScrollController initializeIngredientScrollController() {
    return fixedExtentIngredientScrollController = FixedExtentScrollController(
      initialItem: currentIngredientItem,
    );
  }

  bool handleIngredientScrollNotification(scrollNotification) {
    ///verification of scroll activity
    if (scrollNotification is UserScrollNotification &&
        scrollNotification.direction == ScrollDirection.idle) {
      final isScrollable = true;

      ///item picked in scroll for animation
      Future.delayed(
        Duration.zero,
        () async {
          if (!isScrollable) return;
          fixedExtentIngredientScrollController
            ..animateToItem(
              currentIngredientItem,
              duration: Duration(milliseconds: 1000),
              curve: Curves.bounceOut,
            );
        },
      );
    }
    return false;
  }

  bool handleStepScrollNotification(scrollNotification) {
    ///verification of scroll activity
    if (scrollNotification is UserScrollNotification &&
        scrollNotification.direction == ScrollDirection.idle) {
      final isScrollable = true;

      ///item picked in scroll for animation
      Future.delayed(
        Duration.zero,
        () async {
          if (!isScrollable) return;
          fixedExtentStepScrollController
            ..animateToItem(
              currentStepItem,
              duration: Duration(milliseconds: 1000),
              curve: Curves.bounceOut,
            );
        },
      );
    }
    return false;
  }

  int achieveCurrentIngredient(int item) {
    currentIngredientItem = item;

    ///notifyListeners is needed to animate currentIngredientItem with scrollController
    notifyListeners();
    return currentIngredientItem;
  }

  int achieveCurrentStep(int item) {
    currentStepItem = item;

    ///notifyListeners is needed to animate currentStepItem with scrollController
    notifyListeners();
    return currentStepItem;
  }
}
