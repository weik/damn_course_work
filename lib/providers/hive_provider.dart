import 'package:flutter/material.dart';
import 'package:meals_app/model/category.dart';
import 'package:meals_app/model/meal.dart';
import 'package:meals_app/repository/meals_repository.dart';

class HiveProvider extends ChangeNotifier {
  HiveProvider({this.mealsRepository});
  MealsRepository mealsRepository;
  List<Category> categoryList;
  List<Meal> mealList;

  Future<void> fillHiveDB() async {
    await mealsRepository.putMealsToHive();
    notifyListeners();
  }

  Future<void> getMealsFromHive() async {
    final hiveModel = await mealsRepository.getMealsFromHive();
    categoryList = hiveModel.categoryList;
    mealList = hiveModel.mealsList;
    notifyListeners();
  }

  // void getMeals() async {
  //   final ha = mealsRepository.hiveModel.categoryList[0].id;
  //
  //   print(ha);
  // }
}
