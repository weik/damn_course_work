import 'package:hive/hive.dart';
import 'package:meals_app/data/dummy_data.dart';
import 'package:meals_app/model/hive_model.dart';

class MealsRepository {
  Map<String, List> map;
  Box hiveBox;

  Map<dynamic, dynamic> getMap;

  Future<HiveModel> putMealsToHive() async {
    HiveModel hiveModel = HiveModel();

    hiveBox = Hive.box('meals');
    hiveModel.mealsList = dummyMeals;
    hiveModel.categoryList = dummyCategories;
    // = Hive.box('meals');
    await hiveBox.add(hiveModel);

    hiveModel = hiveBox.getAt(0);

    return hiveModel;
  }

  Future<HiveModel> getMealsFromHive() async {
    HiveModel hiveModel = HiveModel();

    final box = Hive.box('meals');
    hiveModel = box.getAt(0);
    return hiveModel;
  }
}
