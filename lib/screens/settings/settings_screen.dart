import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:meals_app/main.dart';
import 'package:meals_app/screens/home/widget/sidebar_widget.dart';

class SettingsScreen extends StatefulWidget {
  static const id = 'filters_screen';

  final Function saveFilters;
  final Map<String, bool> currentFilters;

  SettingsScreen({this.currentFilters, this.saveFilters});

  @override
  _SettingsScreenState createState() => _SettingsScreenState();
}

class _SettingsScreenState extends State<SettingsScreen> {
  bool _glutenFree = false;
  bool _vegetarian = false;
  bool _vegan = false;
  bool _lactoseFree = false;

  @override
  initState() {
    _glutenFree = widget.currentFilters['gluten'];
    _lactoseFree = widget.currentFilters['lactose'];
    _vegetarian = widget.currentFilters['vegetarian'];
    _vegan = widget.currentFilters['vegan'];
    super.initState();
  }

  Widget _buildSwitchListTile(
    String title,
    String description,
    bool currentValue,
    Function updateValue,
  ) {
    return SwitchListTile(
      activeColor: Theme.of(context).accentColor,
      title: Text(title),
      value: currentValue,
      subtitle: Text(
        description,
      ),
      onChanged: updateValue,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: FloatingActionButton(
        backgroundColor: Colors.red,
        foregroundColor: Theme.of(context).accentColor,
        onPressed: () {
          final selectedFilters = {
            'gluten': _glutenFree,
            'lactose': _lactoseFree,
            'vegan': _vegan,
            'vegetarian': _vegetarian,
          };
          widget.saveFilters(selectedFilters);
        },
        child: Icon(
          Icons.save,
          color: Colors.white,
        ),
      ),
      body: SidebarWidget(
        isFilters: true,
        body: Column(
          children: <Widget>[
            Container(
              margin: EdgeInsets.symmetric(
                  vertical: 16, horizontal: kIsWeb ? 0 : 16),
              height: kIsWeb ? 200 : 100,
              width: kIsWeb
                  ? MediaQuery.of(context).size.width / 2
                  : MediaQuery.of(context).size.width,
              alignment: Alignment.center,
              decoration: BoxDecoration(
                  color: Colors.grey, borderRadius: BorderRadius.circular(25)),
              child: Text(
                'Adjust your meal selection',
                textAlign: TextAlign.center,
                style: Theme.of(context)
                    .textTheme
                    .bodyText1
                    .copyWith(fontSize: kIsWeb ? 32 : 18),
              ),
            ),
            Expanded(
              child: ListView(
                padding: EdgeInsets.symmetric(horizontal: 16, vertical: 8),
                children: <Widget>[
                  _buildSwitchListTile(
                    'Gluten-free',
                    'Only include gluten-free meals.',
                    _glutenFree,
                    (newValue) {
                      setState(
                        () {
                          _glutenFree = newValue;
                        },
                      );
                    },
                  ),
                  _buildSwitchListTile(
                    'Lactose-free',
                    'Only include lactose-free meals.',
                    _lactoseFree,
                    (newValue) {
                      setState(
                        () {
                          _lactoseFree = newValue;
                        },
                      );
                    },
                  ),
                  _buildSwitchListTile(
                    'Vegetarian',
                    'Only include vegetarian meals.',
                    _vegetarian,
                    (newValue) {
                      setState(
                        () {
                          _vegetarian = newValue;
                        },
                      );
                    },
                  ),
                  _buildSwitchListTile(
                    'Vegan',
                    'Only include vegan meals.',
                    _vegan,
                    (newValue) {
                      setState(
                        () {
                          _vegan = newValue;
                        },
                      );
                    },
                  ),
                  _buildSwitchListTile(
                    isDarkMode ? 'Light Mode' : 'Dark Mode',
                    isDarkMode
                        ? 'Turn on to switch to the light mode'
                        : 'Turn on to switch to the dark mode',
                    isDarkMode,
                    (newValue) {
                      setState(
                        () {
                          isDarkMode = newValue;
                        },
                      );
                    },
                  ),
                  Padding(
                    padding: const EdgeInsets.all(16.0),
                    child: Text(
                      'Note.\nIn order to save changes, please press button with the save icon.',
                      textAlign: TextAlign.start,
                      style: Theme.of(context).textTheme.bodyText1.copyWith(
                            fontSize: 10,
                          ),
                    ),
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
