import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:meals_app/model/meal.dart';
import 'package:meals_app/screens/meals/widgets/meal_item_widget.dart';

class MealsScreen extends StatefulWidget {
  static const String id = '/meals_screen';
  final List<Meal> availableMeals;

  MealsScreen(this.availableMeals);

  @override
  _MealsScreenState createState() => _MealsScreenState();
}

class _MealsScreenState extends State<MealsScreen> {
  String categoryTitle;
  List<Meal> displayedMeals;
  var _loadedInitData = false;
  Color categoryColor = Colors.black;
  @override
  void initState() {
    // ...
    super.initState();
  }

  @override
  void didChangeDependencies() {
    if (!_loadedInitData) {
      final mealsArguments =
          ModalRoute.of(context).settings.arguments as Map<String, dynamic>;
      categoryTitle = mealsArguments['title'];
      final categoryId = mealsArguments['id'];
      categoryColor = mealsArguments['color'];
      displayedMeals = widget.availableMeals.where((meal) {
        return meal.categories.contains(categoryId);
      }).toList();
      _loadedInitData = true;
    }
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: categoryColor,
        title: Text(
          categoryTitle,
          textAlign: TextAlign.left,
        ),
      ),
      body: Center(
        child: Container(
          height: double.infinity,
          width: 500,
          alignment: Alignment.topCenter,
          child: ListView.builder(
            itemCount: displayedMeals.length,
            itemBuilder: (_, index) {
              return Center(
                child: ConstrainedBox(
                  constraints: BoxConstraints(
                      maxHeight: kIsWeb ? 450 : 350,
                      maxWidth: kIsWeb ? 450 : 350),
                  child: MealItemWidget(
                    color: categoryColor,
                    id: displayedMeals[index].id,
                    title: displayedMeals[index].title,
                    imageUrl: displayedMeals[index].imageUrl,
                    affordability: Affordability
                        .Affordable, //displayedMeals[index].affordability,
                    complexity:
                        Complexity.Simple, //displayedMeals[index].complexity,
                    duration: displayedMeals[index].duration,
                  ),
                ),
              ); //Text('${categoryMeals[index].title}');
            },
          ),
        ),
      ),
    );
  }
}
