import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:meals_app/data/dummy_data.dart';
import 'package:meals_app/main.dart';
import 'package:meals_app/providers/meals_details_provider.dart';
import 'package:provider/provider.dart';

class MealsDetailScreen extends StatelessWidget {
  static const id = '/meals_details_screen';

  MealsDetailScreen({
    this.toggleFavorite,
    this.isFavorite,
  });
  final Function toggleFavorite;
  final Function isFavorite;

  @override
  Widget build(BuildContext context) {
    final mealId =
        ModalRoute.of(context).settings.arguments as Map<String, dynamic>;
    final selectedMeal =
        dummyMeals.firstWhere((meal) => meal.id == mealId['id'] as String);
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
            icon: Icon(
              Icons.arrow_back_ios,
              color: isDarkMode ? Colors.white : Colors.black,
            ),
            onPressed: () {
              defaultItemsValue(context);
              Navigator.of(context).pop();
            }),
        backgroundColor: isDarkMode ? Colors.black : Colors.white,
        title: Text(
          '${selectedMeal.title}',
          style: Theme.of(context)
              .textTheme
              .bodyText1
              .copyWith(color: isDarkMode ? Colors.white : Colors.black),
        ),
      ),
      body: SafeArea(
        child: Center(
          child: Container(
            width: kIsWeb ? 500 : MediaQuery.of(context).size.width,
            margin: const EdgeInsets.all(15),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(35)),
              boxShadow: [
                BoxShadow(
                  color: Colors.grey.withOpacity(0.9),
                  spreadRadius: 6,
                  blurRadius: 9,
                  offset: Offset(0, 9), // changes position of shadow
                ),
              ],
            ),
            child: Stack(
              children: <Widget>[
                Align(
                  alignment: Alignment(0, -1),
                  child: Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(35)),
                      color: Colors.white,
                    ),
                    alignment: Alignment(0, -1),
                    height: MediaQuery.of(context).size.height / 3,
                    width: double.infinity,
                    child: ClipRRect(
                      borderRadius: kIsWeb
                          ? BorderRadius.all(Radius.circular(35))
                          : BorderRadius.only(
                              topLeft: Radius.circular(35),
                              topRight: Radius.circular(35)),
                      child: Image.network(
                        selectedMeal.imageUrl,
                        width: double.infinity,
                        height: kIsWeb ? 600 : 300,
                        fit: BoxFit.cover,
                        loadingBuilder: (BuildContext context, Widget child,
                            ImageChunkEvent loadingProgress) {
                          if (loadingProgress == null) return child;
                          return Center(
                            child: CircularProgressIndicator(
                              backgroundColor: Colors.amber,
                              value: loadingProgress.expectedTotalBytes != null
                                  ? loadingProgress.cumulativeBytesLoaded /
                                      loadingProgress.expectedTotalBytes
                                  : null,
                            ),
                          );
                        },
                      ),
                    ),
                  ),
                ),
                Align(
                  alignment: Alignment(0, 1),
                  child: Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(35)),
                      color: isDarkMode
                          ? Theme.of(context).primaryColor
                          : Colors.white,
                    ),
                    height: MediaQuery.of(context).size.height / 1.8,
                    width: double.infinity,
                    child: ListView(
                      primary: true,
                      padding: const EdgeInsets.all(16),
                      children: [
                        Center(
                            child: buildSectionTitle(context, 'Ingredients')),
                        ingredientsListWidget(context, selectedMeal,
                            isDarkMode ? Colors.white : Colors.black),
                        Center(child: buildSectionTitle(context, 'Steps')),
                        stepsListWidget(context, selectedMeal,
                            isDarkMode ? Colors.white : Colors.black),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        backgroundColor: Theme.of(context).accentColor,
        child: Icon(
          Icons.favorite_outlined,
          color: isFavorite(mealId['id']) ? Colors.red : Colors.white,
        ),
        onPressed: () => toggleFavorite(mealId['id']),
      ),
    );
  }

  Widget buildSectionTitle(BuildContext context, String text) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 12),
      child: Text(
        text,
        style: Theme.of(context).textTheme.subtitle1,
      ),
    );
  }

  void defaultItemsValue(BuildContext context) {
    final mealItemProvider =
        Provider.of<MealsDetailsProvider>(context, listen: false);
    mealItemProvider.currentIngredientItem = 1;
    mealItemProvider.currentStepItem = 0;
  }

  Widget ingredientsListWidget(
      BuildContext context, dynamic selectedMeal, Color color) {
    final mealItemProvider = Provider.of<MealsDetailsProvider>(context);
    return Container(
      decoration: BoxDecoration(
        color: isDarkMode ? Colors.grey[800] : Colors.white,
        border: Border.all(color: Colors.grey),
        borderRadius: BorderRadius.circular(10),
      ),
      margin: EdgeInsets.all(10),
      padding: EdgeInsets.all(10),
      height: 200,
      width: 300,
      child: NotificationListener(
        onNotification: (onNotification) =>
            mealItemProvider.handleIngredientScrollNotification(onNotification),
        child: ListWheelScrollView.useDelegate(
          perspective: 0.000001,
          squeeze: 1.7,
          itemExtent: 100,
          controller: mealItemProvider.initializeIngredientScrollController(),
          onSelectedItemChanged: (item) =>
              mealItemProvider.achieveCurrentIngredient(item),
          childDelegate: ListWheelChildBuilderDelegate(
            builder: (ctx, index) => Center(
              child: Container(
                width: 250,
                decoration: BoxDecoration(
                  color: color,
                  borderRadius: BorderRadius.circular(8),
                ),
                constraints: BoxConstraints(maxHeight: 80),
                child: Padding(
                  padding: EdgeInsets.symmetric(
                    vertical: 5,
                    horizontal: 10,
                  ),
                  child: Text(
                    selectedMeal.ingredients[index],
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        fontWeight: FontWeight.w500,
                        fontSize: 16,
                        color: isDarkMode ? Colors.black : Colors.white),
                  ),
                ),
              ),
            ),
            childCount: selectedMeal.ingredients.length,
          ),
        ),
      ),
    );
  }

  Widget stepsListWidget(
      BuildContext context, dynamic selectedMeal, Color color) {
    final mealItemProvider = Provider.of<MealsDetailsProvider>(context);
    return Container(
      decoration: BoxDecoration(
        color: isDarkMode ? Colors.grey[800] : Colors.white,
        border: Border.all(color: Colors.grey),
        borderRadius: BorderRadius.circular(10),
      ),
      margin: EdgeInsets.all(10),
      padding: EdgeInsets.all(10),
      height: 300,
      width: 300,
      child: NotificationListener(
        onNotification: (onNotification) =>
            mealItemProvider.handleStepScrollNotification(onNotification),
        child: ListWheelScrollView.useDelegate(
          perspective: 0.001,
          squeeze: 1.7,
          itemExtent: 250,
          controller: mealItemProvider.initializeStepScrollController(),
          onSelectedItemChanged: (item) =>
              mealItemProvider.achieveCurrentStep(item),
          childDelegate: ListWheelChildBuilderDelegate(
            builder: (ctx, index) => Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  SizedBox(height: 10),
                  Divider(),
                  ListTile(
                    leading: CircleAvatar(
                      backgroundColor: color,
                      child: Text(
                        '# ${(index + 1)}',
                        style: TextStyle(
                            fontWeight: FontWeight.w500,
                            fontSize: 18,
                            color: isDarkMode ? Colors.black : Colors.white),
                      ),
                    ),
                    title: Text(
                      selectedMeal.steps[index],
                      style: TextStyle(
                        fontWeight: FontWeight.w500,
                        fontSize: 16,
                        color: isDarkMode ? Colors.white : Colors.black,
                      ),
                    ),
                  ),
                  SizedBox(height: 5),
                ],
              ),
            ),
            childCount: selectedMeal.steps.length,
          ),
        ),
      ),
    );
  }
}
