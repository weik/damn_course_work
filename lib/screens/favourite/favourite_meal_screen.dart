import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:meals_app/model/meal.dart';
import 'package:meals_app/screens/home/widget/sidebar_widget.dart';
import 'package:meals_app/screens/meals/widgets/meal_item_widget.dart';

class FavouriteMealScreen extends StatelessWidget {
  FavouriteMealScreen({this.favouriteMeals});
  final List<Meal> favouriteMeals;

  static const String id = 'favourite_meal_screen';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SidebarWidget(
        isFavourites: true,
        body: Center(
          child: favouriteMeals.isEmpty
              ? Text(
                  'You have no favorites yet - start adding some!',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      fontWeight: FontWeight.w400,
                      fontSize: kIsWeb ? 45 : 22,
                      color: Colors.black,
                      fontStyle: FontStyle.normal),
                )
              : Container(
                  height: double.infinity,
                  width: 500,
                  alignment: Alignment.topCenter,
                  child: ListView.builder(
                    itemCount: favouriteMeals.length,
                    itemBuilder: (_, index) {
                      return Center(
                        child: ConstrainedBox(
                          constraints: BoxConstraints(
                              maxHeight: kIsWeb ? 450 : 350,
                              maxWidth: kIsWeb ? 450 : 350),
                          child: MealItemWidget(
                            id: favouriteMeals[index].id,
                            title: favouriteMeals[index].title,
                            imageUrl: favouriteMeals[index].imageUrl,
                            affordability: Affordability
                                .Affordable, //favouriteMeals[index].affordability,
                            complexity: Complexity
                                .Simple, // favouriteMeals[index].complexity,
                            duration: favouriteMeals[index].duration,
                          ),
                        ),
                      ); //Text('${categoryMeals[index].title}');
                    },
                  ),
                ),
        ),
      ),
    );
  }
}
