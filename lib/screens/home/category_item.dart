import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:meals_app/main.dart';

import '../meals/meals_screen.dart';

class CategoryItem extends StatelessWidget {
  // ignore: use_key_in_widget_constructors
  CategoryItem({this.myColor, this.title, this.id});
  final String id;
  final String myColor;
  final String title;
  Color color;

  Color getColor() {
    Color newColor = isDarkMode ? Colors.black : Colors.white;
    if (myColor == 'pink') {
      newColor = Colors.pink;
      //return newColor;
    }
    if (myColor == 'teal') {
      newColor = Colors.teal;
      //return newColor;
    }
    if (myColor == 'lightGreen') {
      newColor = Colors.lightGreen;
      //return newColor;
    }
    if (myColor == 'lightBlue') {
      newColor = Colors.lightBlue;
      // return newColor;
    }
    if (myColor == 'green') {
      newColor = Colors.green;
      // return newColor;
    }
    if (myColor == 'blue') {
      newColor = Colors.blue;
      //return newColor;
    }
    if (myColor == 'purple') {
      newColor = Colors.purple;
      // return newColor;
    }
    if (myColor == 'red') {
      newColor = Colors.red;
      //return newColor;
    }
    if (myColor == 'orange') {
      newColor = Colors.orange;
      //return newColor;
    }
    if (myColor == 'amber') {
      newColor = Colors.amber;
      // return newColor;
    }
    return newColor;
  }

  @override
  Widget build(BuildContext context) {
    getColor();
    return InkWell(
      borderRadius: BorderRadius.circular(15),
      onTap: () => Navigator.pushNamed(
        context,
        MealsScreen.id,
        arguments: {
          'title': title,
          'id': id,
          'color': getColor(),
        },
      ),
      child: Container(
        padding: const EdgeInsets.all(15),
        decoration: BoxDecoration(
          gradient: LinearGradient(colors: [
            getColor()..withOpacity(0.7),
            getColor(),
          ], begin: Alignment.topLeft, end: Alignment.bottomRight),
          borderRadius: BorderRadius.circular(15),
          color: getColor(),
        ),
        child: GridTile(
          child: Text(
            title,
            style: Theme.of(context).textTheme.headline4.copyWith(
                fontSize: kIsWeb ? 48 : 26, fontWeight: FontWeight.w500),
          ),
        ),
      ),
    );
  }
}
