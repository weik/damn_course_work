import 'package:flutter/material.dart';
import 'package:meals_app/screens/home/widget/sidebar_widget.dart';

import 'slivers/my_appbar.dart';
import 'slivers/my_grid.dart';

// ignore: use_key_in_widget_constructors
class HomeScreen extends StatelessWidget {
  static const String id = '/';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SidebarWidget(
        isHome: true,
        body: Padding(
          padding: const EdgeInsets.fromLTRB(15, 10, 15, 10),
          child: CustomScrollView(
            slivers: [
              SliverPadding(
                padding: const EdgeInsets.fromLTRB(10, 0, 10, 10),
                sliver: MySliverAppBar(),
              ),
              SliverPadding(
                  padding: const EdgeInsets.all(10), sliver: MySliverGrid()),
            ],
          ),
        ),
      ),
    );
  }
}
