import 'package:flutter/material.dart';
import 'package:simple_animations/simple_animations.dart';
import 'package:supercharged/supercharged.dart';

enum _AnimatedColors { color1, color2 }

class AnimatedBackground extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final tween = MultiTween<_AnimatedColors>()
      ..add(_AnimatedColors.color1,
          const Color(0xffD38312).tweenTo(Colors.lightBlue.shade900))
      ..add(_AnimatedColors.color2,
          const Color(0xffA83279).tweenTo(Colors.blue.shade900));

    // ignore: deprecated_member_use
    return MirrorAnimation<MultiTweenValues<_AnimatedColors>>(
      tween: tween,
      duration: const Duration(seconds: 3),
      builder: (context, child, value) {
        return Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(
                  15,
                ),
                topRight: Radius.circular(15)),
            gradient: LinearGradient(
              begin: Alignment.topCenter,
              end: Alignment.bottomCenter,
              colors: [
                value.get(_AnimatedColors.color1),
                value.get(_AnimatedColors.color2),
              ],
            ),
          ),
        );
      },
    );
  }
}
