import 'dart:math';

import 'package:flutter/material.dart';
import 'package:simple_animations/simple_animations.dart';
import 'package:supercharged/supercharged.dart';

import 'curve_painter.dart';

class AnimatedWave extends StatelessWidget {
  // ignore: use_key_in_widget_constructors
  const AnimatedWave({this.height, this.speed, this.offset});

  final double height;
  final double speed;
  final double offset;

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      // ignore: sized_box_for_whitespace
      builder: (_, constrains) => Container(
        height: height,
        width: constrains.biggest.width,
        child: LoopAnimation<double>(
          duration: (5000 / speed).round().milliseconds,
          tween: 0.0.tweenTo(2 * pi),
          builder: (_, child, value) => CustomPaint(
            foregroundPainter: CurvePainter(value: value + offset),
          ),
        ),
      ),
    );
  }
}
