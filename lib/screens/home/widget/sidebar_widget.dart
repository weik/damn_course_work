import 'package:collapsible_sidebar/collapsible_sidebar.dart';
import 'package:flutter/material.dart';
import 'package:meals_app/providers/hive_provider.dart';
import 'package:meals_app/screens/favourite/favourite_meal_screen.dart';
import 'package:meals_app/screens/settings/settings_screen.dart';
import 'package:provider/provider.dart';

import '../../../main.dart';
import '../home_screen.dart';

class SidebarWidget extends StatelessWidget {
  SidebarWidget({
    @required this.body,
    this.isFavourites = false,
    this.isHome = false,
    this.isFilters = false,
  });
  final Widget body;
  final bool isHome;
  final bool isFavourites;
  final bool isFilters;

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: CollapsibleSidebar(
        fitItemsToBottom: true,
        screenPadding: 10,
        backgroundColor:
            isDarkMode ? Colors.grey[700] : const Color(0xff2B3138),
        selectedIconBox:
            isDarkMode ? Colors.deepPurple : const Color(0xff2F4047),
        selectedIconColor: const Color(0xffF3F7F7),
        selectedTextColor: const Color(0xffF3F7F7),
        unselectedIconColor:
            isDarkMode ? Colors.white : const Color(0xff6A7886),
        unselectedTextColor:
            //isDarkMode ? Colors.lime[800] :
            const Color(0xffC0C7D0),
        avatarImg: isDarkMode
            ? const AssetImage('assets/black_logo.png')
            : const AssetImage('assets/white_logo.png'),
        title: 'The Meals',
        items: [
          CollapsibleItem(
            icon: Icons.home,
            text: 'Home',
            onPressed: () async {
              final provider =
                  Provider.of<HiveProvider>(context, listen: false);
              await provider.getMealsFromHive();
              Navigator.pushReplacementNamed(context, HomeScreen.id);
            },
            isSelected: isHome,
          ),
          CollapsibleItem(
            isSelected: isFavourites,
            icon: Icons.favorite,
            text: 'Favourites',
            onPressed: () =>
                Navigator.pushReplacementNamed(context, FavouriteMealScreen.id),
          ),
          CollapsibleItem(
            icon: Icons.settings,
            text: 'Filters',
            onPressed: () =>
                Navigator.pushReplacementNamed(context, SettingsScreen.id),
            isSelected: isFilters,
          ),
        ],
        body: body,
      ),
    );
  }
}
