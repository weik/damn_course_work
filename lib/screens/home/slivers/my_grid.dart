import 'package:flutter/material.dart';
import 'package:meals_app/providers/hive_provider.dart';
import 'package:provider/provider.dart';

import '../../home/category_item.dart';

// ignore: use_key_in_widget_constructors
class MySliverGrid extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final provider = Provider.of<HiveProvider>(context);
    return SliverGrid(
      gridDelegate: const SliverGridDelegateWithMaxCrossAxisExtent(
        mainAxisSpacing: 25,
        crossAxisSpacing: 35,
        maxCrossAxisExtent: 400,
        childAspectRatio: 3 / 2,
      ),
      delegate: SliverChildBuilderDelegate(
        (_, int index) {
          return CategoryItem(
            myColor: provider
                .categoryList[index].color, //dummyCategories[index].color,
            title: provider.categoryList[index].title,
            id: provider.categoryList[index].id,
          );
        },
        childCount: provider.categoryList.length,
      ),
    );
  }
}
