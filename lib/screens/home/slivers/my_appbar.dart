import 'dart:math';

import 'package:flutter/material.dart';
import 'package:meals_app/screens/home/app_bar_animation/animated_wave.dart';
import 'package:meals_app/screens/home/app_bar_animation/app_bar_animation.dart';

import '../../../constants.dart';

// ignore: use_key_in_widget_constructors
class MySliverAppBar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SliverAppBar(
      leading: const SizedBox(),
      backgroundColor: Colors.white,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(15.0),
          topRight: Radius.circular(15),
        ),
      ),
      forceElevated: true,
      expandedHeight: 250,
      flexibleSpace: FlexibleSpaceBar(
        background: Stack(
          children: <Widget>[
            Positioned.fill(
              child: AnimatedBackground(),
            ),
            onBottom(const AnimatedWave(
              height: 120,
              speed: 1.0,
              offset: 5,
            )),
            onBottom(const AnimatedWave(
              height: 100,
              speed: 0.8,
              offset: pi,
            )),
            onBottom(const AnimatedWave(
              height: 60,
              speed: 1.2,
              offset: pi / 2,
            )),
            Align(
              alignment: Alignment(0, 0),
              child: Center(
                child: Text(
                  appName,
                  style: TextStyle(
                    color: Colors.white,
                    fontFamily: 'RobotoCondenses',
                    fontSize: 10,
                  ),
                  textScaleFactor: 5,
                ),
              ),
            ),
          ],
        ), //AnimatedBackground(),
      ),
    );
  }

  Widget onBottom(Widget child) => Align(
        alignment: Alignment.bottomCenter,
        child: child,
      );
}
