import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:meals_app/providers/hive_provider.dart';
import 'package:meals_app/screens/home/home_screen.dart';
import 'package:provider/provider.dart';

class SplashScreen extends StatefulWidget {
  static const String id = 'splash_screen';

  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    openHiveBox();
    super.initState();

    Future.delayed(Duration(seconds: 2), () async {
      if (Hive.box('meals').isEmpty) {
        await awaitFillHive();
      } else {
        await getQuestionsHive();
      }
      Navigator.pushReplacementNamed(
        context,
        HomeScreen.id,
      );
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
          child: CircularProgressIndicator(
        backgroundColor: Colors.lightGreen,
      )),
    );
  }

  Future<void> openHiveBox() async {
    await Hive.openBox('meals');
  }

  Future<void> awaitFillHive() async {
    final provider = Provider.of<HiveProvider>(context, listen: false);
    await provider.fillHiveDB();
  }

  Future<void> getQuestionsHive() async {
    final provider = Provider.of<HiveProvider>(context, listen: false);
    provider.getMealsFromHive();
  }
}
